package ru.tsc.tambovtsev.tm.api;

import ru.tsc.tambovtsev.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
package ru.tsc.tambovtsev.tm.api;

public interface ICommandController {

    void showCommands();

    void showArguments();

    void showSystemInfo();

    void showErrorArgument(String arg);

    void showErrorCommand(String arg);

    void displayWelcome();

    void showAbout();

    void showVersion();

    void showHelp();

}

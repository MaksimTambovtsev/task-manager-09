package ru.tsc.tambovtsev.tm.api;

import ru.tsc.tambovtsev.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
